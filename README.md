
# Previna bem

O Previna bem App é um aplicativo inovador voltado para a área de medicina preventiva. Ele recebe dados biomédicos de pacientes e utiliza modelos de machine learning para gerar uma probabilidade de desenvolvimento de doenças. Essa abordagem proativa permite que profissionais de saúde identifiquem riscos potenciais e forneçam intervenções preventivas personalizadas.

## Pré-requisitos

Antes de começar, certifique-se de ter instalado as seguintes ferramentas:

- [Node.js](https://nodejs.org/) (v18.10.0
)
- [npm](https://www.npmjs.com/) (8.19.2
)
- [Angular CLI](https://cli.angular.io/) (npm install -g @angular/cli@latest)

## Instalação

1. Clone este repositório: `git clone https://gitlab.com/hack-for-change-city-solutions/previnabem.git`
2. Navegue até o diretório do projeto: `cd previna-bem`
3. Instale as dependências: `npm install`

## Como Usar

1. Inicie o servidor de desenvolvimento: `ng serve`
2. Abra o navegador e vá para [http://localhost:4200/](http://localhost:4200/)

O aplicativo será recarregado automaticamente se você alterar qualquer um dos arquivos de origem.


## Algumas informações:

1. Não foi feito a integração com banco de dados, com isso temos alguns usuarios mocados
        email: "maria.oliveira@email.com",
        password: "senha456",

        email: "carlos.pereira@email.com",
        password: "senha789",

2. A integração com API só foi feita no final do projeto com isso não conseguimos cadastrar usuarios com as informações medicas necessarias para fazer a request para o endpoint, com isso fizemos um mock de dados para executar duas requests, uma para a projeção de ataque cardiado e outra para diabetes.

Você pode encontrar esse objeto em panel.component e alterar os valores deles para ver a requests sendo atualizadas.

const paramsDiabetes = {
      gender: 'Male',
      age: '55',
      heart_disease: '1',
      smoking_history: 'current',
      bmi: '25.5',
      HbA1c_level: '6.6',
      blood_glucose_level: '20',
      hypertension: '1',
    };

const paramsCardiac = {
    age: '85',
    sex: '1',
    cp: '3',
    trtbps: '120',
    chol: '233',
    fbs: '1',
    restecg: '0',
    thalachh: '150',
    exng: '0',
    oldpeak: '2.3',
    slp: '0',
    caa: '0',
    thall: '1',
};
