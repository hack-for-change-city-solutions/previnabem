import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { RegistrationService } from '../services/registration.service';
import { User } from '../models/user';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  name: string = '';
  username: string = '';
  password: string = '';
  cpf: string = '';
  email: string = '';


  submitted = false;

  constructor(private router: Router , private userService : RegistrationService  ){

  }

  onSubmit() {
    console.log('Usuário:', this.username);
    console.log('Senha:', this.password);
  }

  createAccount(){
    // this.saveUser();
    // this.router.navigate(['/login']);
  }

  saveUser(): void {
    // debugger;
    // this.userService.create(this.user).then(() => {
    //   console.log('Created new item successfully!');
    //   this.submitted = true;
    // });
  }

  newUser(): void {
    // this.submitted = false;
    // this.user = new User();
  }
  
}
