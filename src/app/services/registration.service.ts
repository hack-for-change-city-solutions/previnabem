import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/compat/database';
import { User } from '../models/user';
@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  private dbPath = '/users';

  userList: AngularFireList<User>;

  constructor(private db: AngularFireDatabase) {
    this.userList = this.db.list(this.dbPath);
  }

  create(user: User): any {
    return this.userList.push(user);
  }

}
