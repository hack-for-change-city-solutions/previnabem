import { Injectable } from '@angular/core';
import { UserList } from '../models/user-list';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  userList: UserList = {
    users: [
      {
        name: "João Silva",
        age: 30,
        email: "admin",
        cpf: "123.456.789-01",
        password: "admin",
        height: 175,
        isSmoker: false,
        weight: 70,
        isHypertension: false,
        heartProblem: false,
        glucose: 90,
        glycatedHemoglobin: 5.5,
        results: {
          imc: {
            value: 22.86,
            degree: "Normal",
          },
          diabetes: {
            probability: 10,
          },
          heartCondition: {
            probability: 5,
          },
        },
      },
      {
        name: "Maria Oliveira",
        age: 28,
        email: "maria.oliveira@email.com",
        cpf: "987.654.321-01",
        password: "senha456",
        height: 160,
        isSmoker: true,
        weight: 60,
        isHypertension: false,
        heartProblem: false,
        glucose: 85,
        glycatedHemoglobin: 5.0,
        results: {
          imc: {
            value: 23.44,
            degree: "Normal",
          },
          diabetes: {
            probability: 15,
          },
          heartCondition: {
            probability: 8,
          },
        },
      },
      {
        name: "Carlos Pereira",
        age: 45,
        email: "carlos.pereira@email.com",
        cpf: "111.222.333-44",
        password: "senha789",
        height: 180,
        isSmoker: false,
        weight: 85,
        isHypertension: true,
        heartProblem: false,
        glucose: 100,
        glycatedHemoglobin: 6.2,
        results: {
          imc: {
            value: 26.23,
            degree: "Sobrepeso",
          },
          diabetes: {
            probability: 25,
          },
          heartCondition: {
            probability: 12,
          },
        },
      },
      {
        name: "Ana Santos",
        age: 35,
        email: "ana.santos@email.com",
        cpf: "555.666.777-88",
        password: "senhaabc",
        height: 165,
        isSmoker: true,
        weight: 55,
        isHypertension: false,
        heartProblem: true,
        glucose: 110,
        glycatedHemoglobin: 7.0,
        results: {
          imc: {
            value: 20.20,
            degree: "Abaixo do peso",
          },
          diabetes: {
            probability: 18,
          },
          heartCondition: {
            probability: 30,
          },
        },
      },
      {
        name: "Pedro Lima",
        age: 40,
        email: "pedro.lima@email.com",
        cpf: "999.888.777-66",
        password: "senhadef",
        height: 175,
        isSmoker: false,
        weight: 75,
        isHypertension: false,
        heartProblem: false,
        glucose: 95,
        glycatedHemoglobin: 5.8,
        results: {
          imc: {
            value: 24.49,
            degree: "Normal",
          },
          diabetes: {
            probability: 12,
          },
          heartCondition: {
            probability: 5,
          },
        },
      },
    ],
  };

  private localStorageKey = 'currentUser';

  constructor() { }


  // Método para salvar informações do usuário localmente
  saveUserLocally(user: any): void {
    // Converte o objeto do usuário para uma string JSON e armazena no localStorage
    localStorage.setItem(this.localStorageKey, JSON.stringify(user));
  }

  // Método para obter informações do usuário armazenadas localmente
  getUserLocally(): any | null {
    // Obtém a string JSON do localStorage
    const storedUser = localStorage.getItem(this.localStorageKey);

    // Se existir, converte a string JSON de volta para um objeto e retorna
    return storedUser ? JSON.parse(storedUser) : null;
  }

  // Método para remover as informações do usuário armazenadas localmente
  removeUserLocally(): void {
    localStorage.removeItem(this.localStorageKey);
  }

  login(email: string, password: string): boolean {
    debugger;
    const userFound = this.userList.users.find((user) => {
      return user.email === email && user.password === password;
    });
    this.saveUserLocally(userFound);
    console.log('User que foi buscado');
    console.log(this.getUserLocally());
    console.log(this.userList);

    return !!userFound;
  }
}
