import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class MachineLearningServiceService {
  private apiUrl = 'http://127.0.0.1:8001'; // Substitua pela URL real da sua API

  constructor(private http: HttpClient) {}

  getData(params: any): Observable<any> {
    debugger;
    // Construir os parâmetros da consulta
    let queryParams = new HttpParams();
    Object.keys(params).forEach((key) => {
      queryParams = queryParams.append(key, params[key]);
    });

    // Fazer a solicitação GET com os parâmetros
    return this.http.get<any>(`${this.apiUrl}/predict_diabetes/`, { params: queryParams });
  }

  getDataHeart(params: any): Observable<any> {
    debugger;
    // Construir os parâmetros da consulta
    let queryParams = new HttpParams();
    Object.keys(params).forEach((key) => {
      queryParams = queryParams.append(key, params[key]);
    });

    // Fazer a solicitação GET com os parâmetros
    return this.http.get<any>(`${this.apiUrl}/predict_heart_attack/`, { params: queryParams });
  }
}
