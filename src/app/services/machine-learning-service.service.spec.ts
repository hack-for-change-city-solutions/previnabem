import { TestBed } from '@angular/core/testing';

import { MachineLearningServiceService } from './machine-learning-service.service';

describe('MachineLearningServiceService', () => {
  let service: MachineLearningServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MachineLearningServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
