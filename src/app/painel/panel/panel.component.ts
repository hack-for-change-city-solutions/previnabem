import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { MachineLearningServiceService } from 'src/app/services/machine-learning-service.service';


interface MachineData {
  // Defina a estrutura esperada dos dados da máquina
  // Exemplo: propriedades 'name', 'status', 'id', etc.
  name: string;
  status: string;
  id: number;
  // ...
}
@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})
export class PanelComponent implements OnInit {

  data:any;
  dataHeart:any;

  user: any = {};
  imcValue: number = 0;
  imcDescription: string = '';


  mensageBaixoRiscoDiabetes: string = 'Recebemos os resultados da sua análise e ficamos felizes em informar que você está em baixo risco de desenvolver diabetes! Continue mantendo um estilo de vida saudável e fazendo escolhas conscientes para garantir seu bem-estar a longo prazo.'

  mensageMedioRiscoDiabetes: string = 'Os resultados da sua análise indicam um nível moderado de risco de desenvolver diabetes. Recomendamos que você adote hábitos ainda mais saudáveis, como fazer exercícios regularmente e manter uma dieta balanceada. Pequenas mudanças podem fazer uma grande diferença!'

  mensageAltoRiscoDiabetes: string = 'Os resultados da sua análise revelam um nível mais elevado de risco de diabetes. É crucial agir agora para evitar complicações futuras. Recomendamos que você marque uma consulta com um profissional de saúde para discutir um plano de ação personalizado e implementar mudanças significativas em seu estilo de vida.'

  diabetesRisk: string = '';
  diabetesRiskCor: string = 'verde';


  mensageBaixoRiscoCardiac: string = 'Ficamos felizes em compartilhar que os resultados indicam que você está em baixo risco de ter um ataque cardíaco. Continue cuidando bem do seu coração com hábitos saudáveis e regulares check-ups médicos.'

  mensageMedioRiscoCardiac: string = 'Os resultados da sua análise mostram um nível moderado de risco de ataque cardíaco. É um sinal para intensificar ainda mais os cuidados com sua saúde cardiovascular. Considere ajustes em sua dieta e rotina de exercícios para fortalecer o coração.'

  mensageAltoRiscoCardiac: string = 'Os resultados revelam um nível mais elevado de risco de ataque cardíaco. Agir agora é crucial. Recomendamos que agende uma consulta com um profissional de saúde para discutir medidas preventivas urgentes. Priorize mudanças significativas no estilo de vida para proteger seu coração.'

  cardiacRisk: string = '';
  cardiacRiskCor: string = 'vermelho';

  constructor(private loginService: LoginService, private machineService: MachineLearningServiceService){}

  ngOnInit(): void {
    this.user = this.loginService.getUserLocally();
    this.imcValue = this.user.results.imc.value;
    this.imcDescription = this.user.results.imc.degree;
    this.diabetesRiskCor = 'verde';

    const paramsDiabetes = {
      gender: 'Male',
      age: '55',
      heart_disease: '1',
      smoking_history: 'current',
      bmi: '25.5',
      HbA1c_level: '6.6',
      blood_glucose_level: '20',
      hypertension: '1',
    };

    const paramsCardiac = {
      age: '85',
      sex: '1',
      cp: '3',
      trtbps: '120',
      chol: '233',
      fbs: '1',
      restecg: '0',
      thalachh: '150',
      exng: '0',
      oldpeak: '2.3',
      slp: '0',
      caa: '0',
      thall: '1',
    };

    this.machineService.getData(paramsDiabetes).subscribe(
      (result: MachineData) => {
        debugger;
        this.data = result;
        console.log(this.data);
        this.calculateRiskDiabetes();
      },
      (error) => {
        console.error('Erro ao obter dados da máquina:', error);
      }
    );

    this.machineService.getDataHeart(paramsCardiac).subscribe(
      (result: MachineData) => {
        debugger;
        this.dataHeart = result;
        console.log(this.data);
        this.calculateRiskCardiac();
      },
      (error) => {
        console.error('Erro ao obter dados da máquina:', error);
      }
    );
  }

  calculateRiskDiabetes(){
    let value =  this.data.probabilidade_diabetes;
    let valuePercent = (value * 100);
    if(valuePercent <= 20) {
      this.diabetesRiskCor = 'verde';
    }else if(valuePercent > 20 && valuePercent < 70){
      this.diabetesRiskCor = 'amarelo';
    }else{
      this.diabetesRiskCor = 'vermelho';
    }
    this.diabetesRisk = valuePercent.toFixed(2);
  }

  calculateRiskCardiac(){
    let value = this.dataHeart.probabilidade_heart_attack;
    let valuePercent = (value * 100);
    if(valuePercent <= 20) {
      this.cardiacRiskCor = 'verde';
    }else if(valuePercent > 20 && valuePercent < 70){
      this.cardiacRiskCor = 'amarelo';
    }else{
      this.cardiacRiskCor = 'vermelho';
    }
    this.cardiacRisk = valuePercent.toFixed(2);
  }

}
