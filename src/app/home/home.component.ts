import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  selectedMenuItem: string = 'panel';

  constructor(){}

  showPanel() {
    this.selectedMenuItem = 'panel';
  }

  showUserData() {
    this.selectedMenuItem = 'userData';
  }

}
