export interface ImcResult {
    value: number;
    degree: string;
}
