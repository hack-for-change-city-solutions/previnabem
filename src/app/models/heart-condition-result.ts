export interface HeartConditionResult {
    probability: number;
}
