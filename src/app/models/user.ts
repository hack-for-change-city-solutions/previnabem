import { DiabetesResult } from "./diabetes-result";
import { HeartConditionResult } from "./heart-condition-result";
import { ImcResult } from "./imc-result";

export interface User {
    cpf: string;
    email: string;
    password: string;
    name: string;
    age: number;
    height: number;
    isSmoker: boolean;
    weight: number;
    isHypertension: boolean;
    heartProblem: boolean;
    glucose: number;
    glycatedHemoglobin: number;
    results: {
      imc: ImcResult;
      diabetes: DiabetesResult;
      heartCondition: HeartConditionResult;
    };
}
