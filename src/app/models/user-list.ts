import { User } from "./user";

export interface UserList {
    users: User[];
}
