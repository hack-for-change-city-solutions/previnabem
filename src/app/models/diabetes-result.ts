export interface DiabetesResult {
    probability: number;
}
