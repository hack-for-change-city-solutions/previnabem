import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() status: string = 'gray'; // Recebe o status para determinar a cor da borda
  @Input() percent: string = '';

  constructor(){}

  ngOnInit(): void {
    console.log(this.percent);
    console.log(this.status);
  }

}
