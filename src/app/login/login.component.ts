import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  email: string = '';
  password: string = '';
  isErroLogin: boolean = false;
  constructor(private router: Router, private loginService: LoginService) {

  }

  onSubmit() {
  }

  login() {
    let isLogin = this.loginService.login(this.email, this.password);
    if (isLogin) {
      this.router.navigate(['/home']);
    } else {
      this.isErroLogin = true;
    }
  }

  createAccount() {
    this.router.navigate(['/register']);
  }

}
